#pragma once


namespace photo_validator
{

    class PhotoGroup;
    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

    class TemplateObj;
    typedef QSharedPointer<TemplateObj> TemplateObjShp;

    class PhotoDownloadManager : public QObject
    {
        Q_OBJECT

    public:
        PhotoDownloadManager(QObject *parent = nullptr);
        ~PhotoDownloadManager();

    public slots:
        void start(const PhotoGroupShp& group);
        void startDownloadTemplate(QList<TemplateObjShp> photosList);
        void test(QList<TemplateObjShp> photosList);
        void slotError(QNetworkReply::NetworkError err);
        void slotSslErrors(QList<QSslError> err);

    private:
        QString saveFileName(const QUrl &url);

    signals:
        void end(const PhotoGroupShp& group);
        void failed(const PhotoGroupShp& group);
        void endTemplate(QList<TemplateObjShp> photosList);
        void signalIsBusy(const PhotoGroupShp& group);

    private slots:
        void startNextDownload();
        void downloadFinished();
        void downloadReadyRead();

    private:
        PhotoGroupShp _group;
        QNetworkAccessManager *_manager;
        QQueue<QUrl> _downloadQueue;
        QNetworkReply *_currentReply;
        QFile _output;

        QStringList _listPath;

        const quint8 PHOTOS_COUNT = 6;

        bool stateDownloadManager;
        bool stateDownloadPhotoDriver = false;
        bool stateDownloadNow = false;
        QList<TemplateObjShp> _photosList;
        QStringList _commentList;

        QString _filename;

        quint8 _idPhoto;
        QMap<QUrl, QString > _mapTemplate;
        QUrl _url;
    };

}
