#include "Common.h"
#include "PhotoDownloadManager.h"

#include "PhotoGroup.h"
#include "TemplateObj.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"


using namespace photo_validator;

PhotoDownloadManager::PhotoDownloadManager(QObject* parent)
    : QObject(parent)
    , _manager(new QNetworkAccessManager(this))
    , _idPhoto(0)
{
}

PhotoDownloadManager::~PhotoDownloadManager()
{
}

void PhotoDownloadManager::start(const PhotoGroupShp& group)
{
    if (stateDownloadNow)
    {
        emit signalIsBusy(group);
        return;
    }

    stateDownloadNow = true;
    stateDownloadManager = true;
    stateDownloadPhotoDriver = false;
    _group = group;
    _listPath.clear();

    if(_group->_photoUrls.isEmpty())
        return;

    for (const auto& url : _group->_photoUrls)
        _downloadQueue.enqueue(url);
//    _downloadQueue.enqueue(_group->_urlPhotoDriver);
    startNextDownload();
}

void PhotoDownloadManager::startDownloadTemplate(QList<TemplateObjShp> photosList)
{
    stateDownloadManager = false;
    _photosList = photosList;
    for(const auto& pair : _photosList)
    {
        _downloadQueue.enqueue(pair->_url);
        _commentList << pair->_comment;
    }
    startNextDownload();
}

void PhotoDownloadManager::test(QList<TemplateObjShp> photosList)
{
    stateDownloadManager = false;
    _photosList = photosList;
    for (const auto& pair : _photosList)
    {
        _downloadQueue.enqueue(pair->_url);
        _commentList << pair->_comment;
    }
    startNextDownload();
}

void PhotoDownloadManager::slotError(QNetworkReply::NetworkError err)
{
    qDebug() << __FUNCTION__ << err;
    qDebug() << _url.toString();
    startNextDownload();
}

void PhotoDownloadManager::slotSslErrors(QList<QSslError> err)
{
    qDebug() << __FUNCTION__ << err;
}

QString PhotoDownloadManager::saveFileName(const QUrl &url)
{
    const auto path = url.path();

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto& imagesFolder = settings["ImagesFolder"].toString();

    QDir imagesDir(QDir::homePath());
    if (!imagesDir.exists(imagesFolder))
    {
        const auto mkpathResult = imagesDir.mkpath(imagesFolder);
        if (!mkpathResult)
        {
            Q_ASSERT_X(mkpathResult, __FUNCTION__, "imagesDir.mkpath == false");
            return QString();
        }
    }
    imagesDir.cd(imagesFolder);

    QString basename;
    if (stateDownloadPhotoDriver)
        basename = QString("%1/%2.%3").arg(imagesDir.absolutePath()).arg(QFileInfo(path).fileName()).arg("jpg");
    else
        basename = QString("%1/%2").arg(imagesDir.absolutePath()).arg(QFileInfo(path).fileName());

    if (basename.isEmpty())
        basename = "download";

    return basename;
}

void PhotoDownloadManager::startNextDownload()
{
    if (_downloadQueue.isEmpty())
    {
        if (stateDownloadManager)
        {
            if (stateDownloadPhotoDriver)
            {
                if(_listPath.count())
                    _group->_downloadPhotoDriver = _listPath.last();

                emit end(_group);
                stateDownloadNow = false;
                return;
            }

            for (const auto& path : _listPath)
                _group->_downloadPhotos << path;

            if (_group->_urlPhotoDriver.isEmpty())
            {
                emit end(_group);
                stateDownloadNow = false;
                return;
            }

            stateDownloadPhotoDriver = true;

            _downloadQueue.enqueue(_group->_urlPhotoDriver);
            _listPath.clear();
        }
        else
        {
            for (auto elemetTemplate : _photosList)
            {
                QMapIterator<QUrl, QString> element(_mapTemplate);
                while (element.hasNext())
                {
                    element.next();
                    if (elemetTemplate->_url == element.key())
                    {
                        elemetTemplate->_path = element.value();
                        break;
                    }
                }
            }

            emit endTemplate(_photosList);
            return;
        }
    }

    _url = _downloadQueue.dequeue();

    if (!_output.exists() && !_url.isEmpty())
    {
        QNetworkRequest request(_url);
        _currentReply = _manager->get(request);

        connect(_currentReply, &QNetworkReply::finished,
                this, &PhotoDownloadManager::downloadFinished);

        connect(_currentReply, &QNetworkReply::readyRead,
                this, &PhotoDownloadManager::downloadReadyRead);

        connect(_currentReply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
                this, &PhotoDownloadManager::slotError);

        connect(_currentReply, &QNetworkReply::sslErrors,
                this, &PhotoDownloadManager::slotSslErrors);
    }
    else
    {
        if (!_commentList.isEmpty())
        {
            _mapTemplate[_url] = _filename;
            ++_idPhoto;
        }

        _listPath << _filename;
        startNextDownload();
    }
}

void PhotoDownloadManager::downloadFinished()
{
//    _output.close();

    if (_currentReply->error())
    {
        // download failed
        emit failed(_group);
        qDebug() << "Failed: " + _currentReply->errorString() + "\n";
        _currentReply->deleteLater();
        _currentReply = nullptr;
        _downloadQueue.clear();
        return;
    }
    else
    {
        if (!_commentList.isEmpty())
        {
            _mapTemplate[_url] = /*_output.fileName();*/_filename;
            ++_idPhoto;
        }

        _listPath << /*_output.fileName();*/ _filename;
    }

    _currentReply->deleteLater();
    _currentReply = nullptr;
    startNextDownload();
}

void PhotoDownloadManager::downloadReadyRead()
{
    QString name = saveFileName(_url);
    QFile file(name);
    _filename = file.fileName();
    if (file.open(QIODevice::Append))
        file.write(_currentReply->readAll());
    else
        qDebug() << "ERROR SyKA";
    file.close();
}
