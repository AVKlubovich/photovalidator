#include "Common.h"
#include "TemplateObj.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"


using namespace photo_validator;

TemplateObj::TemplateObj(const QString &url, const QString &comment)
    : _url(url)
    , _comment(comment)
{
}

RegisterMetaType(QSharedPointer<photo_validator::TemplateObj>)
