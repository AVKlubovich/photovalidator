#pragma once


namespace Ui
{
    class HistoryWidget;
}

namespace photo_validator
{

    class PhotoGroup;
    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

    class HistoryWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit HistoryWidget(const PhotoGroupShp group, QWidget *parent = nullptr);
        ~HistoryWidget();

    signals:
        void requestPhotos(const PhotoGroupShp& group);

    private slots:
        void on_btnRequestPhotos_clicked();

        void clickOnImage(QGraphicsPixmapItem *pixmapItem);

        void on_btnPhotoDriver_clicked();

    private:
        void createSceneImage();
        void fillInfo();

    private:
        Ui::HistoryWidget *_ui;

        PhotoGroupShp _group;

        QVector<QGraphicsItem*> _vectorItems;

        const int WIDTH_PIXMAP = 240;
        const int HEIGHT_PIXMAP = 180;
        const int OFSET_ITEMS = 5;
        const int HEIGHT_TOOLBUTTON = 30;
    };

}
