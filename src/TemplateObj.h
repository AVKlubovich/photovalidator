#pragma once


namespace photo_validator
{

    class TemplateObj
    {
    public:
        TemplateObj(const QString& url, const QString& comment);

    public:
        QUrl _url;
        QString _comment;
        QString _path;
    };

    typedef QSharedPointer<TemplateObj> TemplateObjShp;

}

Q_DECLARE_METATYPE(QSharedPointer<photo_validator::TemplateObj>);
