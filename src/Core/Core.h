﻿#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace utils
{
    class Logger;
    typedef QSharedPointer<Logger> LoggerShp;
}

namespace photo_validator
{

    class Controller;
    typedef QSharedPointer<Controller> ControllerShp;

    class Core
        : public utils::Singleton<Core>
    {
    public:
        Core();
        ~Core() = default;

        bool init();
        void run();
        void done();

    private:
        void readConfig();
        bool initLogger();
        bool initSession();
        bool initWindowController();

    private:
        utils::LoggerShp _logger;
        session_controller::SessionShp _session;
        photo_validator::ControllerShp _windowController;
    };

}
