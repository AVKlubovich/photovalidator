﻿#include "Common.h"
#include "Core.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/Logging/Logger.h"
#include "utils/Logging/LoggerMessages.h"
#include "utils/Logging/Devices/FileDevice.h"
#include "utils/Logging/Devices/DebuggerDevice.h"
#include "utils/Logging/Devices/ConsoleDevice.h"

#include "database/DBManager.h"
#include "database/DatabaseCreator.h"

#include "session-controller/Session.h"
#include "session-controller/SessionController.h"

#include "Controller.h"


using namespace photo_validator;

Core::Core()
{
}

bool Core::init()
{
    readConfig();

    if (!initLogger())
    {
        qDebug() << "Logger does not initialize";
        return false;
    }

    if (!initSession())
    {
        qDebug() << "Session does not initialize";
        return false;
    }

    if (!initWindowController())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    return true;
}

void Core::run()
{
    _windowController->run();
}

void Core::done()
{
    _windowController.clear();
    _session.clear();
    _logger.clear();
}

void Core::readConfig()
{
    QDir::setCurrent( QCoreApplication::applicationDirPath() );

    utils::Settings::Options config = { "configuration/photo-validator.ini", true };
    utils::SettingsFactory::instance().registerSettings("client", config);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings =
    {
        // Client
        { "Client/FetchDataInterval", 5000 },
        { "Client/ImagesFolder", "PhotoValidatorImages" },
        { "Client/SoundTimeout", 60000 },

        // Connection
        { "Connection/Scheme", "http" },
        { "Connection/Host", "192.168.213.56" },
        { "Connection/Port", "81" },

        // Logs
        { "Log/FlushInterval", 1000 },
        { "Log/PrefixName", "photo_validator.log" },
        { "Log/Dir", "PhotoValidator_logs" },
        { "Log/MaxSize", 134217728 }, // 100 mb
    };

    utils::SettingsFactory::instance().setCurrentSettings("client");
}

bool Core::initLogger()
{
    return true;
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toInt();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();

    QDir logDir(QDir::homePath());
    const auto& logFolder = settings["Dir"].toString();
    if (!logDir.exists(logFolder))
    {
        if (!logDir.mkdir(logFolder))
            return false;
    }
    logDir.cd(logFolder);

    fileOptions->prefixName = settings["PrefixName"].toString();
    fileOptions->directory = logDir.absolutePath();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;

    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;

    qDebug() << "initLogger";
    return true;
}

bool Core::initSession()
{
    _session = session_controller::SessionShp::create();
    auto sessionController = session_controller::SessionControllerShp::create(_session);

#ifdef QT_DEBUG
    QString login("test_andrej");
    QString password("andrttest");
    sessionController->setDefaultCredentials(login, password);
#endif

    return sessionController->requestUserCredentials();
}

bool Core::initWindowController()
{
    _windowController = ControllerShp::create();
    _windowController->setSession(_session);

    return true;
}
