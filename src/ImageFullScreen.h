#pragma once


namespace Ui
{
    class ImageFullScreen;
}

namespace photo_validator
{

    class PhotoGroup;
    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

    class ImageFullScreen : public QWidget
    {
        Q_OBJECT

    public:
        explicit ImageFullScreen(const PhotoGroupShp group, int pos, QWidget *parent = nullptr);
        explicit ImageFullScreen(const QString &path, QWidget *parent = nullptr);
        ~ImageFullScreen();

        void setPath(const QString& path);

    private slots:
        void on_btnRotationLeft_clicked();
        void on_btnRotationRight_clicked();

        void on_btnLeft_clicked();

        void on_btnRight_clicked();

        void on_btnSaveImage_clicked();

    protected:
        void keyPressEvent(QKeyEvent *event) override;
        bool eventFilter(QObject *obj, QEvent *event) override;

    private:
        Ui::ImageFullScreen *_ui;
        QGraphicsPixmapItem *_pixmapItem;
        QStringList listPath;
        int _pos;
    };

}
