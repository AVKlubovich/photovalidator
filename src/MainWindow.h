#pragma once


namespace Ui
{
    class MainWindow;
}

namespace photo_validator
{

    class TemplateWidget;

    class TemplateObj;
    typedef QSharedPointer<TemplateObj> TemplateObjShp;

    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        void addInfoWidget(QWidget *newWidget);
        void addHistoryWidget(QWidget *newWidget);

        void searchList(QStringList &list);

        void setStatistic(const QString &date, QVariantList body);
        void onLoadTemplate(QList<TemplateObjShp> urlList);

        void setPermissions(const QList<QVariant> permissions);
        void setCountReccordsOperator(const quint64 count);

    public slots:
        void checkLifeFeed();
        void checkHistory();

    private slots:
        void on_tabWidget_currentChanged(int index);
        void on_btnFint_clicked();
        void on_btnCreateStatistic_clicked();
        void onLoadTemplateClicked();
        void onEditTemplate(const QString &comment);
        void onAddTemplate(const QString &comment);
        void onDeleteTemplate();
        void on_btnAddPhoto_clicked();
        void on_btnLoadTemplate_clicked();
        void on_cbParameters_activated(int index);

    signals:
        void refreshTableModel();
        void selectHistory(const QVariantMap parameters);
        void selectStatistic(const QVariantMap parameters);
        void selectTemplate();

        void loadTemplate();
        void addPhotoTemplate(const QString& newPhotoUrl, const QString &comment);
        void editPhotoTemplate(const QString& newPhotoUrl,  const QString &comment, const quint8 pos);
        void deletePhotoTemplate(const quint8 pos);

        void signalSearch(int field);

    protected:
        void closeEvent(QCloseEvent* event);

    private:
        void createTemplate(const QString &photoUrl, const QString &comment = QString(""));

    private:
        Ui::MainWindow *_ui;
        QVBoxLayout _layoutLifeFeed;
        QVBoxLayout _layoutHistory;
        QHBoxLayout _layoutTemplate;

        QLabel* _emptyLabelForLifeFeed = nullptr;
        QLabel* _emptyLabelForHistory = nullptr;

        QList<TemplateWidget *> _templateWidgets;

        int _userRights;
    };

}
