#pragma once


namespace Ui
{
    class InfoWidget;
}

namespace photo_validator
{

    class PhotoGroup;
    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

    class InfoWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit InfoWidget(const PhotoGroupShp group, QWidget *parent = nullptr);
        ~InfoWidget();

    signals:
        void accept(const PhotoGroupShp& group);
        void inquiry(const PhotoGroupShp& group);
        void blocked(const PhotoGroupShp& group);

    public slots:
        void onCheckValidity(const quint64 groupId);

    private slots:
        void checkVisibleForWidgets();

        void on_btnAccept_clicked();
        void on_btnInquiry_clicked();
        void on_btnBlock_clicked();

        void clickOnImage(QGraphicsPixmapItem* pixmapItem);

        void on_btnPhotoDriver_clicked();

    private:
        void createSceneImage(const QString& driverPhoto, const QStringList& listPathImage);
        void fillInfo();

    private:
        Ui::InfoWidget *_ui;

        PhotoGroupShp _group;

        QVector<QToolButton*> _vectorToolButton;
        QVector<QGraphicsItem*> _vectorItems;

        const int WIDTH_PIXMAP = 240;
        const int HEIGHT_PIXMAP = 180;
        const int OFSET_ITEMS = 5;
        const int HEIGHT_TOOLBUTTON = 30;
    };

}
