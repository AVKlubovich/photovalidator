#pragma once


namespace photo_validator
{

    class PhotoGroup
    {
    public:
        PhotoGroup();

        quint64 _groupId;
        quint64 _driverId;
        QString _driverName;
        QString _driverNumber;
        quint64 _autoId;
        QString _autoModel;
        QString _autoMarka;
        QString _autoNumber;
        QString _autoColor;
        QDateTime _dateCreate;
        QDateTime _dateClose;

        QVector<QUrl> _photoUrls;
        QStringList _downloadPhotos;

        QUrl _urlPhotoDriver;
        QString _downloadPhotoDriver;

        quint8 _status = 0;
        QSet<quint8> _failedPhotos;
        const QVariantList getFailedPhotos() const;
        QString _userName;
        QString _comment;

        // NOTE: auto options
        quint8 _autoClass;
        const QString getAutoClass() const;

        quint8 _branding = 0;
        quint8 _terminal = 0;
        quint8 _chair = 0;
        quint8 _chairCradle = 0;
        quint8 _booster = 0;
        quint8 _airport = 0;
        quint8 _checker = 0;

        quint8 _countDownload = 0;

        qreal _verified = 0;
        bool isVerified();

        QString toString();

    private:
        QMap<quint8, QString> _autoClasses;
    };

    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

}

Q_DECLARE_METATYPE(QSharedPointer<photo_validator::PhotoGroup>);
