#include "PhotosItem.h"


using namespace photo_validator;

PhotosItem::PhotosItem(const QPixmap &pixmap, QGraphicsItem *parent)
    : QObject()
    , QGraphicsPixmapItem(pixmap, parent)
{
}

void PhotosItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->type() == QEvent::GraphicsSceneMousePress)
    {
        emit signalClickedByImage(this);
    }

    QGraphicsPixmapItem::mousePressEvent(event);
}
