#pragma once


namespace photo_validator
{

    class PhotosItem : public QObject, public QGraphicsPixmapItem
    {
        Q_OBJECT
    public:
        PhotosItem(const QPixmap& pixmap, QGraphicsItem* parent = nullptr);
        ~PhotosItem() = default;

    signals:
        void signalClickedByImage(QGraphicsPixmapItem *watched);

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent * event) override;
    };

}
