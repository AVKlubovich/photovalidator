#include "Common.h"

#include "Core/Core.h"


void setTextCodec(const QString& encodingName)
{
    QTextCodec *codec = QTextCodec::codecForName(encodingName.toStdString().c_str());
    QTextCodec::setCodecForLocale(codec);
}

int main(int argc, char *argv[])
{
    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(".");
    paths.append("platforms");
    paths.append("imageformats");
    paths.append("audio");
    QCoreApplication::setLibraryPaths(paths);

    QApplication a(argc, argv);

    setTextCodec("UTF-8");

    QTranslator translator;
    const auto tsResult = translator.load("translations/language_ru.qm");
    if (tsResult)
        a.installTranslator(&translator);
    else if (translator.load("language_ru.qm"))
        a.installTranslator(&translator);

    const auto fusionStyle = QStyleFactory::create("Fusion");
    a.setStyle(fusionStyle);

    auto& core = photo_validator::Core::instance();
    if (!core.init())
    {
        core.done();
        return -1;
    }

    core.run();

    a.exec();

    core.done();

    return 0;
}
