#include "Common.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "TemplateWidget.h"

#include "TemplateObj.h"


using namespace photo_validator;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , _ui(new Ui::MainWindow)
{
    _ui->setupUi(this);
    _ui->scrollAreaLifeFeed->setLayout(&_layoutLifeFeed);
    _ui->scrollAreaHistory->setLayout(&_layoutHistory);
    _ui->scrollAreaTemplate->setLayout(&_layoutTemplate);

    _ui->dateStartHistory->setDateTime(QDateTime::currentDateTime().addDays(-1));
    _ui->dateEndHistory->setDateTime(QDateTime::currentDateTime());
    _ui->dateStartStatistic->setDateTime(QDateTime::currentDateTime().addDays(-1));
    _ui->dateEndStatistic->setDateTime(QDateTime::currentDateTime());

    _ui->labelCountReccordsOperator->setHidden(true);

    connect(_ui->btnLoadTemplate, &QPushButton::clicked, this, &MainWindow::onLoadTemplateClicked);
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::addInfoWidget(QWidget* newWidget)
{
    if (_emptyLabelForLifeFeed)
    {
        delete _emptyLabelForLifeFeed;
        _emptyLabelForLifeFeed = nullptr;
    }

    _layoutLifeFeed.addWidget(newWidget);
}

void MainWindow::addHistoryWidget(QWidget* newWidget)
{
    if (_emptyLabelForHistory)
    {
        delete _emptyLabelForHistory;
        _emptyLabelForHistory = nullptr;
    }

    _layoutHistory.addWidget(newWidget);
}

void MainWindow::searchList(QStringList &list)
{
    QCompleter *compliter = new QCompleter(list, _ui->editFind);
    compliter->setCaseSensitivity(Qt::CaseInsensitive);
    _ui->editFind->setCompleter(compliter);
}

void MainWindow::setStatistic(const QString& date, QVariantList body)
{
    auto layout = static_cast<QVBoxLayout*> (_ui->widgetStatistic->layout());
    layout->addWidget(new QLabel(date));

    quint64 allCount = 0;
    for (auto element : body)
    {
        auto & map = element.toMap();
        auto & text = QString("%1: %2").arg(map["name"].toString()).arg(map["count"].toString());
        allCount += map["count"].toLongLong();
        layout->addWidget(new QLabel(text));
    }

    const auto & text = "Всего обработанно: " + QString::number(allCount);
    layout->addWidget(new QLabel(text));

    layout->addStretch(1);
}

void MainWindow::onLoadTemplate(QList<TemplateObjShp> urlList)
{
    for (auto templateWidget : _templateWidgets)
    {
        _layoutTemplate.removeWidget(templateWidget);
        templateWidget->deleteLater();
    }

    _templateWidgets.clear();

    for (const auto& photo : urlList)
        createTemplate(photo->_path, photo->_comment);
}

void MainWindow::setPermissions(const QList <QVariant> permissions)
{
    _userRights = permissions.at(0).toInt();
    bool state = !_userRights;
    _ui->btnAddPhoto->setHidden(state);
}

void MainWindow::setCountReccordsOperator(const quint64 count)
{
    _ui->labelCountReccordsOperator->setHidden(false);
    _ui->labelCountReccordsOperator->setText("Количесво записей: " + QString::number(count));
}

void MainWindow::checkLifeFeed()
{
    if (_layoutLifeFeed.isEmpty())
    {
        _emptyLabelForLifeFeed = new QLabel(tr("No new photos"));
        _emptyLabelForLifeFeed->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        _layoutLifeFeed.addWidget(_emptyLabelForLifeFeed);
    }
}

void MainWindow::checkHistory()
{
    if (_layoutHistory.isEmpty())
    {
        _emptyLabelForHistory = new QLabel(tr("Records not found"));
        _emptyLabelForHistory->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        _layoutHistory.addWidget(_emptyLabelForHistory);
    }
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if (index == 3)
        emit refreshTableModel();
}

void MainWindow::on_btnFint_clicked()
{
    enum FilterParametersEnum
    {
        None,
        DriverName,
        DriverId,
        AutoNumber,
        DriverPhoneNumber,
        NameOperator
    };
    const QVector<QString> filterParametersMap
    {
        "none",
        "driver_name",
        "driver_id",
        "auto_number",
        "driver_number",
        "id_user"
    };

    QVariantMap parameters;

    const auto idParameter = _ui->cbParameters->currentIndex();
    if (idParameter != FilterParametersEnum::None)
    {
        parameters.insert(filterParametersMap[idParameter], _ui->editFind->text());
    }

    parameters["date_start"] = _ui->dateStartHistory->dateTime().toString("yyyy-MM-dd hh:mm:ss");
    parameters["date_end"] = _ui->dateEndHistory->dateTime().toString("yyyy-MM-dd hh:mm:ss");
//    parameters["date_end"] = _ui->dateEndHistory->dateTime().addDays(1).addMSecs(-1).toString("yyyy-MM-dd hh:mm:ss");

    emit selectHistory(parameters);
}

void photo_validator::MainWindow::on_btnCreateStatistic_clicked()
{
    QVariantMap parameters;
    parameters["date_start"] = _ui->dateStartStatistic->dateTime().toString("yyyy-MM-dd hh:mm:ss");
    parameters["date_end"] = _ui->dateEndStatistic->dateTime().addDays(1).addMSecs(-1).toString("yyyy-MM-dd hh:mm:ss");

    emit selectStatistic(parameters);
}

void MainWindow::onLoadTemplateClicked()
{
    emit loadTemplate();
}

void MainWindow::onEditTemplate(const QString &comment)
{
    const auto& templateWidget = qobject_cast<TemplateWidget *>(sender());
    const auto pos = _templateWidgets.indexOf(templateWidget);

    emit editPhotoTemplate(templateWidget->getPhotoUrl(), comment, pos);
}

void MainWindow::onAddTemplate(const QString &comment)
{
    const auto& templateWidget = qobject_cast<TemplateWidget *>(sender());

    emit addPhotoTemplate(templateWidget->getPhotoUrl(), comment);
}

void MainWindow::onDeleteTemplate()
{
    const auto& templateWidget = qobject_cast<TemplateWidget *>(sender());
    const auto pos = _templateWidgets.indexOf(templateWidget);
    _templateWidgets.removeAt(pos);
    delete templateWidget;

    emit deletePhotoTemplate(pos);
}

void photo_validator::MainWindow::on_btnAddPhoto_clicked()
{
    const auto& newUrl = QFileDialog::getOpenFileName(this, tr("Open Image"), "/image", tr("Image Files (*.png *.jpg *.jpeg)"));
    if (newUrl.isEmpty())
        return;

    createTemplate(newUrl);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    ::TerminateProcess(::GetCurrentProcess(), 0);
    event->accept();
}

void MainWindow::createTemplate(const QString &photoUrl, const QString& comment)
{
    TemplateWidget *newTemplateWidget = new TemplateWidget(photoUrl, comment, _userRights);

    connect(newTemplateWidget, &TemplateWidget::editPhoto,
            this, &MainWindow::onEditTemplate);
    connect(newTemplateWidget, &TemplateWidget::addPhoto,
            this, &MainWindow::onAddTemplate);
    connect(newTemplateWidget, &TemplateWidget::deletePhoto,
            this, &MainWindow::onDeleteTemplate);

    _templateWidgets << newTemplateWidget;
    _layoutTemplate.addWidget(newTemplateWidget);
}

void photo_validator::MainWindow::on_btnLoadTemplate_clicked()
{
    emit selectTemplate();
}

void photo_validator::MainWindow::on_cbParameters_activated(int index)
{
    bool state = index;
    _ui->labelCountReccordsOperator->setHidden(true);
    if(state)
        emit signalSearch(index);
    else
        searchList(QStringList());
}
