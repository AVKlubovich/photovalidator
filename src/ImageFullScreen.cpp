#include "Common.h"
#include "ImageFullScreen.h"
#include "ui_ImageFullScreen.h"
#include "PhotoGroup.h"


using namespace photo_validator;

ImageFullScreen::ImageFullScreen(const PhotoGroupShp group, int pos, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ImageFullScreen)
    , _pos(pos)
{
    _ui->setupUi(this);

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->showFullScreen();

    listPath << group->_downloadPhotos << group->_downloadPhotoDriver;

    QPixmap icon(listPath.at(_pos));
    QGraphicsScene * scene = new QGraphicsScene(this);
    scene->addRect(scene->sceneRect());
    _pixmapItem = scene->addPixmap(icon);
    _pixmapItem->setPos(-_pixmapItem->pixmap().width() / 2, -_pixmapItem->pixmap().height() / 2);
    _ui->graphicsView->setScene(scene);
    scene->installEventFilter(this);

}

ImageFullScreen::ImageFullScreen(const QString &path, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ImageFullScreen)
{
    _ui->setupUi(this);

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->showFullScreen();

    QPixmap pix(path);

    QGraphicsScene * scene = new QGraphicsScene(this);
    scene->addRect(scene->sceneRect());
    _pixmapItem = scene->addPixmap(pix);
    _pixmapItem->setPos(-_pixmapItem->pixmap().width() / 2, -_pixmapItem->pixmap().height() / 2);
    _ui->graphicsView->setScene(scene);
    scene->installEventFilter(this);

    _ui->btnLeft->setHidden(true);
    _ui->btnRight->setHidden(true);
}

ImageFullScreen::~ImageFullScreen()
{
    delete _ui;
}

void ImageFullScreen::setPath(const QString &path)
{
    QPixmap icon(path);
    QGraphicsScene * scene = new QGraphicsScene(this);
    _pixmapItem = scene->addPixmap(icon);
    _pixmapItem->setPos(-_pixmapItem->pixmap().width() / 2, -_pixmapItem->pixmap().height() / 2);
    _ui->graphicsView->setScene(scene);
    scene->installEventFilter(this);
}

void photo_validator::ImageFullScreen::on_btnRotationLeft_clicked()
{
    _pixmapItem->setRotation(_pixmapItem->rotation() - 90);
    auto scene = _ui->graphicsView->scene();
    scene->setSceneRect(_pixmapItem->sceneBoundingRect());
}

void photo_validator::ImageFullScreen::on_btnRotationRight_clicked()
{
    _pixmapItem->setRotation(_pixmapItem->rotation() + 90);
    auto scene = _ui->graphicsView->scene();
    scene->setSceneRect(_pixmapItem->sceneBoundingRect());
}

void ImageFullScreen::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        this->close();
        event->accept();
    }

    QWidget::keyPressEvent(event);
}

bool ImageFullScreen::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::GraphicsSceneMouseRelease)
    {
        this->close();
        return true;
    }
    return QWidget::eventFilter(obj, event);
}

void photo_validator::ImageFullScreen::on_btnLeft_clicked()
{
    --_pos;
    if (_pos < 0)
        _pos = listPath.count() - 1;
    _ui->graphicsView->scene()->clear();
    QPixmap icon(listPath.at(_pos));
    _pixmapItem = _ui->graphicsView->scene()->addPixmap(icon);
    _ui->graphicsView->scene()->setSceneRect(_pixmapItem->sceneBoundingRect());
}

void photo_validator::ImageFullScreen::on_btnRight_clicked()
{
    ++_pos;
    if (_pos > listPath.count() - 1)
        _pos = 0;
    _ui->graphicsView->scene()->clear();
    QPixmap icon(listPath.at(_pos));
    _pixmapItem = _ui->graphicsView->scene()->addPixmap(icon);
    _ui->graphicsView->scene()->setSceneRect(_pixmapItem->sceneBoundingRect());
}

void photo_validator::ImageFullScreen::on_btnSaveImage_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
             tr("Сохранить файл"), "save_photo_" + QDateTime::currentDateTime()
                                                    .toString("dd_MM_yy_hh_mm_ss"),
             tr("Изображение (*.jpg *.jpeg *.png);; "
                "All Files (*)"));
    if (!fileName.isEmpty())
        _pixmapItem->pixmap().save(fileName);
}
