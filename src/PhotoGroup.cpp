#include "Common.h"
#include "PhotoGroup.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"

RegisterMetaType(QSharedPointer<photo_validator::PhotoGroup>)


using namespace photo_validator;

PhotoGroup::PhotoGroup()
    : _autoClasses(QMap<quint8, QString>
                   {
                       { 1,  QObject::tr("Economy") },
                       { 2,  QObject::tr("Comfort") },
                       { 3,  QObject::tr("Business") },
                       { 4,  QObject::tr("VIP") },
                       { 5,  QObject::tr("Minivan") },
                       { 6,  QObject::tr("Minivan (8 places)") },
                       { 7,  QObject::tr("Minivan VIP") },
                       { 8,  QObject::tr("SUV") },
                       { 9,  QObject::tr("SUV VIP") },
                       { 10, QObject::tr("Platinum") },
                   })
{
}

const QVariantList PhotoGroup::getFailedPhotos() const
{
    QVariantList failedPhotos;
    for (const auto failedPhoto : _failedPhotos)
        failedPhotos << failedPhoto;

    return failedPhotos;
}

const QString PhotoGroup::getAutoClass() const
{
    if (_autoClasses.contains(_autoClass))
        return _autoClasses[_autoClass];
    return QString();
}

QString PhotoGroup::toString()
{
    return (this) ?
                QString("driver id %1, group id %2").arg(_driverId).arg(_groupId)
              : QString("object is null");
}

bool photo_validator::PhotoGroup::isVerified()
{
    return _verified == 1;
}
