#include "Common.h"
#include "TemplateWidget.h"
#include "ui_TemplateWidget.h"


using namespace photo_validator;

TemplateWidget::TemplateWidget(const QString &photoUrl, const QString &comment, const int userRights, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::TemplateWidget)
    , _photoUrl(photoUrl)
    , _bufPhotoUrl(photoUrl)
    , _comment(comment)
{
    _ui->setupUi(this);

    fillPhoto();

    connect(_ui->editComment, &QLineEdit::textChanged, this, &TemplateWidget::statusBtnSave);

    if (userRights == 1)
    {
        connect(_ui->btnImage, &QToolButton::clicked, this, &TemplateWidget::onEditPhotoClicked);
        connect(_ui->btnDelete, &QPushButton::clicked, this, &TemplateWidget::onDeleteClicked);
    }
    else
    {
        _ui->editComment->setReadOnly(true);
        _ui->btnDelete->setVisible(false);
        _ui->btnSave->setVisible(false);
    }
}

TemplateWidget::~TemplateWidget()
{
    delete _ui;
}

const QString TemplateWidget::getPhotoUrl() const
{
    return _photoUrl;
}

void TemplateWidget::onEditPhotoClicked()
{
    const QString& newUrl = QFileDialog::getOpenFileName(this, tr("Open Image"), "/image", tr("Image Files (*.png *.jpg *.jpeg)"));
    if (newUrl.isEmpty())
        return;

    stateBtnSave = true;
    _photoUrl = newUrl;
    fillPhoto();
}

void TemplateWidget::onDeleteClicked()
{
    emit deletePhoto();
}

void TemplateWidget::fillPhoto()
{
    if (_photoUrl.isEmpty())
        return;

    const QSize size(300, 200);
    QPixmap pixmap;
    pixmap.load(_photoUrl);
    pixmap.scaled(size);

    _ui->btnImage->setFixedSize(size);
    _ui->btnImage->setIconSize(size);
    _ui->btnImage->setIcon(pixmap);

    _ui->editComment->setText(_comment);

    this->adjustSize();
    statusBtnSave();
}

void photo_validator::TemplateWidget::on_btnSave_clicked()
{
    if(!stateBtnSave)
    {
        stateBtnSave = true;
        emit addPhoto(_ui->editComment->text());
        return;
    }

    _bufPhotoUrl = _photoUrl;
    _comment = _ui->editComment->text();
    emit editPhoto(_ui->editComment->text());
    _ui->btnSave->setEnabled(false);
}

void TemplateWidget::statusBtnSave()
{
    if ((_comment != _ui->editComment->text()) || (_bufPhotoUrl != _photoUrl))
        _ui->btnSave->setEnabled(true);
    else
        _ui->btnSave->setEnabled(false);
}
