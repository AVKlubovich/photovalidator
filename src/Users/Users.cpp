﻿#include "Common.h"
#include "Users.h"


using namespace photo_validator;

QVariant User::toVariant() const
{
    QVariantMap user;
    user["id"] = _id;
    user["login"] = _login;
    user["name"] = _name;
    user["password"] = _password;
    user["permissions"] = _permission;
    return user;
}

void User::fromVariant(const QVariant & data)
{
    QVariantMap userMap = data.toMap();
    _id = userMap["id"].toLongLong();
    _login = userMap["login"].toString();
    _password = userMap["password"].toString();
    _name = userMap["name"].toString();
    _permission = userMap["permissions"].toInt();
}

long long User::id() const
{
    return _id;
}

void User::setId(long long id)
{
    _id = id;
}

QString User::login() const
{
    return _login;
}

void User::setLogin(const QString &login)
{
    _login = login;
}

QString User::name() const
{
    return _name;
}

void User::setName(const QString &name)
{
    _name = name;
}

int User::permission() const
{
    return _permission;
}

void User::setPermission(const int &permission)
{
    _permission = permission;
}

QString User::password() const
{
    return _password;
}

void User::setPassword(const QString &password)
{
    _password = password;
}

//QVariant Users::toVariant() const
//{
//    QVariantList users;
//    for (const User user: *this)
//    {
//        users.push_back(user.toVariant());
//    }
//    return users;
//}

void Users::fromVariant(QVariant data)
{
    QVariantList usersVariantList = data.toList();
    for (QVariant & userVariant: usersVariantList)
    {
        User user;
        user.fromVariant(userVariant);
        push_back(user);
    }
}
