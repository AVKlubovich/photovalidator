﻿#pragma once


namespace photo_validator
{

    class User
    {
    public:
        explicit User() = default;

        QVariant toVariant() const;
        void fromVariant(const QVariant & data);

        long long id() const;
        void setId(long long id);

        QString login() const;
        void setLogin(const QString &login);

        QString name() const;
        void setName(const QString &name);

        int permission() const;
        void setPermission(const int &permission);

        QString password() const;
        void setPassword(const QString &password);

    private:
        long long _id;
        QString _login;
        QString _password;
        QString _name;
        int _permission;

        //const QUuid _uuid;
    };


    class Users : public QList<User>
    {
    public:
        QVariant toVariant() const
        {
            QVariantList users;
            for (const User user: *this)
            {
                users.push_back(user.toVariant());
            }
            return users;
        }

        void fromVariant(QVariant users);
    };

}
