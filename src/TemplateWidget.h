#pragma once


namespace Ui
{
    class TemplateWidget;
}

namespace photo_validator
{

    class TemplateWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit TemplateWidget(const QString& photoUrl, const QString& comment, const int userRights, QWidget *parent = nullptr);
        ~TemplateWidget();

        const QString getPhotoUrl() const;

    private slots:
        void onEditPhotoClicked();
        void onDeleteClicked();
        void on_btnSave_clicked();

    private:
        void fillPhoto();
        void statusBtnSave();

    signals:
        void deletePhoto();
        void editPhoto(const QString &comment);
        void addPhoto(const QString &comment);

    private:
        Ui::TemplateWidget *_ui;

        QString _photoUrl;
        QString _bufPhotoUrl;
        QString _comment;

        bool stateBtnSave = false; // new photo
    };

}
