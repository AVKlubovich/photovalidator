#include "Common.h"
#include "HistoryWidget.h"
#include "ui_HistoryWidget.h"

#include "PhotoGroup.h"
#include "ImageFullScreen.h"
#include "Models/PhotosItem.h"


using namespace photo_validator;

HistoryWidget::HistoryWidget(const PhotoGroupShp group, QWidget* parent)
    : QWidget(parent)
    , _ui(new Ui::HistoryWidget)
    , _group(group)
{
    _ui->setupUi(this);

    createSceneImage();
    fillInfo();

    _ui->btnPhotoDriver->setHidden(true);
}

HistoryWidget::~HistoryWidget()
{
    delete _ui;
}

void HistoryWidget::createSceneImage()
{
    int ofsetX = 0;

    QGraphicsScene * scene = new QGraphicsScene(this);

    // photo driver

    QStringList listPath;
    listPath << _group->_downloadPhotos << _group->_downloadPhotoDriver;

    for (auto i = 0; i < listPath.count(); ++i)
    {
        const auto& pathImage = listPath.at(i);
        const bool isFailed = _group->_failedPhotos.contains(i);

        QPixmap pixmap(pathImage);
        pixmap = pixmap.scaled(WIDTH_PIXMAP, HEIGHT_PIXMAP, Qt::KeepAspectRatio);
        PhotosItem * pixmapItem = new PhotosItem(pixmap);

        connect(pixmapItem, &PhotosItem::signalClickedByImage,
                this, &HistoryWidget::clickOnImage);

        scene->addItem(pixmapItem);
        auto ofsetY = (HEIGHT_PIXMAP - pixmapItem->pixmap().size().height()) / 2;

        QPushButton *failedBtn = new QPushButton();

        if (isFailed)
            failedBtn->setPalette(QPalette(QColor(Qt::red)));
        else
            failedBtn->setPalette(QPalette(QColor(Qt::green)));

        failedBtn->resize(pixmapItem->pixmap().size().width(), HEIGHT_TOOLBUTTON);

        auto btnScene = scene->addWidget(failedBtn);
        btnScene->setPos(ofsetX, HEIGHT_PIXMAP + OFSET_ITEMS);

        pixmapItem->setPos(ofsetX, ofsetY);

        _vectorItems.append(pixmapItem);

        ofsetX += pixmapItem->pixmap().size().width() + OFSET_ITEMS;
    }
    _ui->graphicsView->setScene(scene);
    _ui->graphicsView->horizontalScrollBar()->setValue(1);
}

void HistoryWidget::fillInfo()
{
    _ui->editDriverId->setText(QString::number(_group->_driverId));
    _ui->editDriverName->setText(_group->_driverName);
    _ui->editDriverNumber->setText(_group->_driverNumber);
    _ui->editAutoMarkaModel->setText(QString("%1 %2").arg(_group->_autoMarka).arg(_group->_autoModel));
    _ui->editAutoNumber->setText(_group->_autoNumber);
    _ui->editAutoColor->setText(_group->_autoColor);
    _ui->editUserName->setText(_group->_userName);
    _ui->editComment->setText(_group->_comment);
    _ui->editAutoClass->setText(_group->getAutoClass());

    const QMap<quint8, QString> statusesMap
    {
        { 0, QObject::tr("New") },
        { 1, QObject::tr("Accept") },
        { 2, QObject::tr("Inquiry") },
        { 3, QObject::tr("Auto accept") },
        { 4, QObject::tr("Block") }
    };
    _ui->labelStatus->setText(QString(QObject::tr("Status: %1").arg(statusesMap[_group->_status])));

    _ui->editDateCreate->setDateTime(_group->_dateCreate);
    _ui->editDateClose->setDateTime(_group->_dateClose);

    enum AutoOptionsToolTipsEnum
    {
        Branding,
        Terminal,
        Chair,
        ChairCradle,
        Booster,
        Airport,
        Checker
    };

    QStringList autoOptionsToolTips
    {
        QObject::tr("Branding"),
        QObject::tr("Terminal"),
        QObject::tr("Chair"),
        QObject::tr("Chair cradle"),
        QObject::tr("Booster"),
        QObject::tr("Airport"),
        QObject::tr("Checker")
    };

    auto addOptionImage = [&, this](const QString& imagePath, const AutoOptionsToolTipsEnum toolTip)
    {
        QPixmap pixmap(imagePath);
        pixmap = pixmap.scaled(25, 25, Qt::IgnoreAspectRatio);
        auto pixLabel = new QLabel(this);
        pixLabel->setPixmap(pixmap);
        pixLabel->setMask(pixmap.mask());
        pixLabel->setToolTip(autoOptionsToolTips.at(toolTip));
        _ui->autoOptionsLayout->addWidget(pixLabel);
    };

    if (_group->_branding)
        addOptionImage(":auto_options/option_branding.png", Branding);

    if (_group->_terminal)
        addOptionImage(":auto_options/option_terminal.png", Terminal);

    if (_group->_chair)
        addOptionImage(":auto_options/option_chair.png", Chair);

    if (_group->_chairCradle)
        addOptionImage(":auto_options/option_chair_cradle.png", ChairCradle);

    if (_group->_booster)
        addOptionImage(":auto_options/option_booster.png", Booster);

    if (_group->_airport)
        addOptionImage(":auto_options/option_airport.png", Airport);

    if (_group->_airport)
        addOptionImage(":auto_options/option_checker.png", Checker);

    _ui->autoOptionsLayout->addSpacing(50);

    if (_group->_downloadPhotoDriver.isEmpty())
        _ui->btnPhotoDriver->setEnabled(false);
}

void photo_validator::HistoryWidget::on_btnRequestPhotos_clicked()
{
    sender()->deleteLater();

    emit requestPhotos(_group);
}

void HistoryWidget::clickOnImage(QGraphicsPixmapItem *pixmapItem)
{
    if(_vectorItems.contains(pixmapItem))
    {
        auto imageFullScreen = new ImageFullScreen(_group, _vectorItems.indexOf(pixmapItem));
        imageFullScreen->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint);
        imageFullScreen->setAttribute(Qt::WA_DeleteOnClose);
        imageFullScreen->show();
    }
}

void photo_validator::HistoryWidget::on_btnPhotoDriver_clicked()
{
    auto imageFullScreen = new ImageFullScreen(_group->_downloadPhotoDriver);
    imageFullScreen->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint);
    imageFullScreen->setAttribute(Qt::WA_DeleteOnClose);
    imageFullScreen->show();
}
