#pragma once


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace photo_validator
{

    class MainWindow;
    typedef QSharedPointer<MainWindow> MainWindowShp;

    class PhotoGroup;
    typedef QSharedPointer<PhotoGroup> PhotoGroupShp;

    class TemplateObj;
    typedef QSharedPointer<TemplateObj> TemplateObjShp;


    class Controller : public QObject
    {
        Q_OBJECT

    public:
        explicit Controller(QObject *parent = nullptr);
        ~Controller();

        void setSession(const session_controller::SessionShp &session);

        void run();

    signals:
        void checkValidity(const quint64 groupId);
        void deleteHistoryWidgets();
        void signalLiveFeedDownloadManager(const PhotoGroupShp& group);
        void signalHistoryDownloadManager(const PhotoGroupShp& group);
        void signalTemplateDownloadManager(QList<TemplateObjShp> photosList);
        void signalTest(QList<TemplateObjShp> photosList);

    private slots:
        void onEndDownload(const PhotoGroupShp& group);
        void onEndDownloadForHistory(const PhotoGroupShp& group);
        void onEndDownloadForTemplate(QList<TemplateObjShp> photosList);

        void onFailedDownload(const PhotoGroupShp& _group);
        void onFailedDownloadForHistory(const PhotoGroupShp &_group);

        void acceptDriver(const PhotoGroupShp& group);
        void inquiryDriver(const PhotoGroupShp& group);
        void blockDriver(const PhotoGroupShp& group);
        void requestDriver(const PhotoGroupShp& group);

        void onSelectHistoryForMain(const QVariantMap parameters);
        void onSelectStatisticForMain(const QVariantMap parameters);

        void addImageInTemplate(const QString &pathPhoto, const QString &comment);
        void editImageInTemplate(const QString &pathPhoto, const QString &comment, const int pos);
        void deleteImageInTemplate(const int pos);
        void onGetReply(QNetworkReply *reply);

        void searchInfo(int idField);
        void isBusy(const PhotoGroupShp& group);

    private:
        void selectNewPhotos();
        void onSelectNewPhotos(network::ResponseShp response);

        void fetchNewPhotos();
        void onFetchNewPhotos(const network::ResponseShp response);

        void onAcceptDriver(network::ResponseShp response);
        void onInquiryDriver(network::ResponseShp response);
        void onBlockDriver(network::ResponseShp response);
        void onRequestDriver(network::ResponseShp response);

        void onSelectHistoryPhotos(network::ResponseShp response);
        void onSelectStatistic(network::ResponseShp response);

        void onSelectTemplate(network::ResponseShp response);
        void onSelectTemplateForMain();

        void onSearchData(const network::ResponseShp response);

        void runFetchTimer();
        void timerEvent(QTimerEvent *event);

        const PhotoGroupShp createGroup(const QVariantMap& data);
        void startDownload();
        void startDownloadForHistory();

        void createDownloadManagers();

        void createMediaPlayer();
        void playNewPhotosSound();

        void networkAccesManager(QHttpMultiPart* multiPart);

    private:
        MainWindowShp _mainWindow;

        network::RequestsManagerShp _requestsManager;
        session_controller::SessionShp _session;

        QQueue<PhotoGroupShp> _groupsQueue;
        QQueue<PhotoGroupShp> _groupsQueueHistory;

        QDateTime _lastFetchDate;

        QThread _threadLiveFeed;
        QThread _threadHistory;
        QThread _threadTemlate;

        QSound *_newPhotosSound = nullptr;
        QElapsedTimer _soundTimer;

        const quint8 MAX_COUNT_DOWNLOADS = 3;
        const int DELAY_START_TIMER = 100;
        const int NO_POSITION_IMAGE_IN_TEMPLATE = -1;

        QMap <QString, quint64> _mapUsers;
    };

    typedef QSharedPointer<Controller> ControllerShp;

}

typedef QList<QPair<QString, QString>> TemplatePair;
Q_DECLARE_METATYPE(TemplatePair);
