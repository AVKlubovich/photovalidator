#include "Common.h"
#include "Controller.h"

#include "MainWindow.h"
#include "InfoWidget.h"
#include "HistoryWidget.h"
#include "ImageFullScreen.h"

#include "PhotoDownload/PhotoDownloadManager.h"
#include "PhotoGroup.h"
#include "TemplateObj.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Response.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"

#include "session-controller/Session.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"

RegisterMetaType(TemplatePair)


using namespace photo_validator;

Controller::Controller(QObject *parent)
    : QObject(parent)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _requestsManager->init();
}

Controller::~Controller()
{
    _threadLiveFeed.quit();
    _threadLiveFeed.wait();

    _threadHistory.quit();
    _threadHistory.wait();

    _threadTemlate.quit();
    _threadTemlate.wait();

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto& imagesFolder = settings["ImagesFolder"].toString();

    QDir imagesDir(QDir::homePath());
    if (imagesDir.exists(imagesFolder))
    {
        imagesDir.cd(imagesFolder);
        imagesDir.removeRecursively();
    }

    _soundTimer.start();
}

void Controller::setSession(const session_controller::SessionShp &session)
{
    _session = session;
}

void Controller::run()
{
    createDownloadManagers();
    createMediaPlayer();

    _mainWindow = MainWindowShp::create();
    _mainWindow->showMaximized();
    _mainWindow->setPermissions(_session->userRights());
    _mainWindow->setWindowTitle(QString("PhotoValidator %1. Приятной работы, %2")
                                .arg("1.20")
                                .arg(_session->userName()));

    connect(_mainWindow.data(), &MainWindow::selectHistory, this, &Controller::onSelectHistoryForMain);
    connect(_mainWindow.data(), &MainWindow::selectStatistic, this, &Controller::onSelectStatisticForMain);
    connect(_mainWindow.data(), &MainWindow::selectTemplate, this, &Controller::onSelectTemplateForMain);

    connect(_mainWindow.data(), &MainWindow::signalSearch, this, &Controller::searchInfo);

    connect(_mainWindow.data(), &MainWindow::editPhotoTemplate, this, &Controller::editImageInTemplate);
    connect(_mainWindow.data(), &MainWindow::addPhotoTemplate, this, &Controller::addImageInTemplate);
    connect(_mainWindow.data(), &MainWindow::deletePhotoTemplate, this, &Controller::deleteImageInTemplate);

    selectNewPhotos();
}

void Controller::onEndDownload(const PhotoGroupShp& group)
{
    InfoWidget *infoW = new InfoWidget(group);
    _mainWindow->addInfoWidget(infoW);

    playNewPhotosSound();

    connect(infoW, &InfoWidget::accept, this, &Controller::acceptDriver);
    connect(infoW, &InfoWidget::inquiry, this, &Controller::inquiryDriver);
    connect(infoW, &InfoWidget::blocked, this, &Controller::blockDriver);
    connect(this, &Controller::checkValidity, infoW, &InfoWidget::onCheckValidity);

    QTimer::singleShot(DELAY_START_TIMER, this, &Controller::startDownload);
}

void Controller::onEndDownloadForHistory(const PhotoGroupShp &group)
{
    HistoryWidget *historyW = new HistoryWidget(group);
    connect(this, &Controller::deleteHistoryWidgets, historyW, &HistoryWidget::deleteLater);
    connect(historyW, &HistoryWidget::requestPhotos, this, &Controller::requestDriver);
    _mainWindow->addHistoryWidget(historyW);

    QTimer::singleShot(DELAY_START_TIMER, this, &Controller::startDownloadForHistory);
}

void Controller::onEndDownloadForTemplate(QList<TemplateObjShp> photosList)
{
    _mainWindow->onLoadTemplate(photosList);
}

void Controller::onFailedDownload(const PhotoGroupShp& _group)
{
    PhotoGroupShp group = _group;
    if (group->_countDownload < MAX_COUNT_DOWNLOADS)
        _groupsQueue.enqueue(group);
    ++group->_countDownload;
    QTimer::singleShot(DELAY_START_TIMER, this, &Controller::startDownload);
}

void Controller::onFailedDownloadForHistory(const PhotoGroupShp& _group)
{
    PhotoGroupShp group = _group;
    if (group->_countDownload < MAX_COUNT_DOWNLOADS)
        _groupsQueueHistory.enqueue(group);
    ++group->_countDownload;
    QTimer::singleShot(DELAY_START_TIMER, this, &Controller::startDownloadForHistory);
}

void Controller::acceptDriver(const PhotoGroupShp& group)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "accept_driver";
    requestMap["id_driver"] = group->_driverId;
    requestMap["driver_name"] = group->_driverName;
    requestMap["group_id"] = group->_groupId;
    requestMap["auto_id"] = group->_autoId;
    requestMap["driver_status"] = 1;
    requestMap["user_login"] = _session->login();
    requestMap["user_pass"] = _session->password();
    requestMap["id_user"] = _session->userId();

    network::RequestShp request(new network::DefaultRequest("accept_driver", requestMap));
    _requestsManager->execute(request, this, &Controller::onAcceptDriver);

    sender()->deleteLater();
}

void Controller::inquiryDriver(const PhotoGroupShp& group)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "accept_driver";
    requestMap["id_driver"] = group->_driverId;
    requestMap["group_id"] = group->_groupId;
    requestMap["auto_id"] = group->_autoId;
    requestMap["driver_name"] = group->_driverName;
    requestMap["driver_status"] = 2;
    requestMap["comment"] = group->_comment;
    requestMap["failed_photos"] = group->getFailedPhotos();
    requestMap["user_login"] = _session->login();
    requestMap["user_pass"] = _session->password();
    requestMap["id_user"] = _session->userId();

    network::RequestShp request(new network::DefaultRequest("accept_driver", requestMap));
    _requestsManager->execute(request, this, &Controller::onInquiryDriver);

    sender()->deleteLater();
}

void Controller::blockDriver(const PhotoGroupShp& group)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "accept_driver";
    requestMap["id_driver"] = group->_driverId;
    requestMap["group_id"] = group->_groupId;
    requestMap["auto_id"] = group->_autoId;
    requestMap["driver_name"] = group->_driverName;
    requestMap["driver_status"] = 4;
    requestMap["comment"] = group->_comment;
    requestMap["failed_photos"] = group->getFailedPhotos();
    requestMap["user_login"] = _session->login();
    requestMap["user_pass"] = _session->password();
    requestMap["id_user"] = _session->userId();

    network::RequestShp request(new network::DefaultRequest("accept_driver", requestMap));
    _requestsManager->execute(request, this, &Controller::onBlockDriver);

    sender()->deleteLater();
}

void Controller::requestDriver(const PhotoGroupShp& group)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "accept_driver";
    requestMap["id_driver"] = group->_driverId;
    requestMap["auto_id"] = group->_autoId;
    requestMap["driver_name"] = group->_driverName;
    requestMap["driver_status"] = 2;
    requestMap["auto_id"] = group->_autoId;
    requestMap["user_login"] = _session->login();
    requestMap["user_pass"] = _session->password();

    network::RequestShp request(new network::DefaultRequest("accept_driver", requestMap));
    _requestsManager->execute(request, this, &Controller::onRequestDriver);
}

void Controller::onSelectHistoryForMain(const QVariantMap parameters)
{
    emit deleteHistoryWidgets();
    _mainWindow->checkHistory();

    QVariantMap requestMap;
    requestMap["type_command"] = "select_history_photos";

    QList<QVariant> statusesList
    {
//        0,
        1,
        2,
        3,
        4
    };
    requestMap["statuses"] = QVariant::fromValue(statusesList);

    for (auto it = parameters.begin(); it != parameters.end(); ++it)
    {
        if (!it.key().compare("id_user"))
        {
            requestMap["filter_name"] = it.key();
            requestMap["filter_value"] = _mapUsers[it.value().toString()];
            break;
        }

        if (it.key().compare("driver_name") == 0 ||
            it.key().compare("driver_id") == 0 ||
            it.key().compare("auto_number") == 0 ||
            it.key().compare("driver_number") == 0)
        {
            requestMap["filter_name"] = it.key();
            requestMap["filter_value"] = it.value();
        }
    }

    requestMap["date_start"] = parameters["date_start"];
    requestMap["date_end"] = parameters["date_end"];

    network::RequestShp request(new network::DefaultRequest("select_history_photos", requestMap));
    _requestsManager->execute(request, this, &Controller::onSelectHistoryPhotos);
}

void Controller::onSelectStatisticForMain(const QVariantMap parameters)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "select_statistic";

    for (auto it = parameters.begin(); it != parameters.end(); ++it)
        requestMap[it.key()] = it.value();

    network::RequestShp request(new network::DefaultRequest("select_statistic", requestMap));
    _requestsManager->execute(request, this, &Controller::onSelectStatistic);
}

void Controller::addImageInTemplate(const QString &pathPhoto, const QString &comment)
{
    // TODO можно переписать на множественную передачу картинок
    /*
        type_command=add_pos
        json=
        {
            "comments":
            {
                "image0":"comment0",
                "image1":"comment1"
            }
        }

        image0=[file]
        image1=[file]
     */
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("add_pos");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const QString json = QString("{\"comments\":{\"image\":\"%1\"}}").arg(comment);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(" form-data; name=\"image\"; filename=\""+pathPhoto+"\" "));
    imagePart.setRawHeader("Content-Transfer-Encoding","binary");
    QFile *file = new QFile(pathPhoto);
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(imagePart);

    networkAccesManager(multiPart);
}

void Controller::editImageInTemplate(const QString &pathPhoto, const QString &comment, const int pos)
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("edit_pos");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const QString json = QString("{\"pos\":\"%1\",\"comment\":\"%2\"}").arg(QString::number(pos)).arg(comment);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(" form-data; name=\"image\"; filename=\""+pathPhoto+"\" "));
    imagePart.setRawHeader("Content-Transfer-Encoding","binary");
    QFile *file = new QFile(pathPhoto);
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(imagePart);

    networkAccesManager(multiPart);
}

void Controller::deleteImageInTemplate(const int pos)
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("delete_pos");
    multiPart->append(typeCommandPart);

    const QString json = QString("{\"pos\":\"%1\"}").arg(QString::number(pos));
    QHttpPart jsonPart;
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    networkAccesManager(multiPart);
}

void Controller::onGetReply(QNetworkReply *reply)
{
    qDebug() << qPrintable(reply->readAll());
}

void Controller::searchInfo(int idField)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "quick_search";
    requestMap["field"] = idField;

    network::RequestShp request(new network::DefaultRequest("quick_search", requestMap));
    _requestsManager->execute(request, this, &Controller::onSearchData);
}

void Controller::isBusy(const PhotoGroupShp &group)
{
    _groupsQueue.enqueue(group);
}

void Controller::selectNewPhotos()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "select_new_photos";

    network::RequestShp request(new network::DefaultRequest("select_new_photos", requestMap));
    _requestsManager->execute(request, this, &Controller::onSelectNewPhotos);
}

void Controller::onSelectNewPhotos(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto& commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "select_new_photos") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"];
    const auto& recordsList = body.toList();

    for (const auto& record : recordsList)
    {
        const auto& map = record.toMap();
        if (map.count() == 1 &&
            map.contains("last_fetch_date"))
        {
            _lastFetchDate = map.value("last_fetch_date").toDateTime();
        }
        else
        {
            const auto& newGroup = createGroup(map);
            if (!newGroup.isNull())
            {
                _groupsQueue.enqueue(newGroup);
            }
        }
    }
    startDownload();

    _mainWindow->checkLifeFeed();

    runFetchTimer();
}

void Controller::fetchNewPhotos()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "fetch_new_photos";
    requestMap["last_fetch_date"] = _lastFetchDate.toString("yyyy-MM-dd hh:mm:ss.zzz");

    network::RequestShp request(new network::DefaultRequest("fetch_new_photos", requestMap));
    _requestsManager->execute(request, this, &Controller::onFetchNewPhotos);
}

void Controller::onFetchNewPhotos(const network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto& commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "fetch_new_photos") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto& otherList = body["other"].toList();
    const auto& newList = body["new"].toList();
    _lastFetchDate = body.value("last_fetch_date").toDateTime();

    for (const auto& removedId : otherList)
    {
        emit checkValidity(removedId.toMap()["id"].toUInt());
        _mainWindow->checkLifeFeed();
    }

    for (const auto& newRecord : newList)
    {
        const auto& newGroup = createGroup(newRecord.toMap());
        _groupsQueue.enqueue(newGroup);
    }
    startDownload();

    _mainWindow->checkLifeFeed();

    runFetchTimer();
}

void Controller::onAcceptDriver(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "accept_driver") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto& driverId = body["id_driver"].toULongLong();
    const auto& date = body["date"].toString();
    const auto& driverName = body["driver_name"].toString();

    qDebug() << QString("Driver %1 ( %2 ) is allowed at %3").arg(driverName).arg(driverId).arg(date);
}

void Controller::onInquiryDriver(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "accept_driver") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto driverId = body["id_driver"].toULongLong();
    const auto& date = body["date"].toString();
    const auto& driverName = body["driver_name"].toString();

    qDebug() << QString("The driver %1 ( %2 ) must send a photo. %3").arg(driverName).arg(driverId).arg(date);

    _mainWindow->checkLifeFeed();
}

void Controller::onBlockDriver(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "accept_driver") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto driverId = body["id_driver"].toULongLong();
    const auto& date = body["date"].toString();
    const auto& driverName = body["driver_name"].toString();

    qDebug() << QString("The driver %1 ( %2 ) must send a photo. %3").arg(driverName).arg(driverId).arg(date);

    _mainWindow->checkLifeFeed();
}

void Controller::onRequestDriver(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "accept_driver") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto driverId = body["id_driver"].toULongLong();
    const auto& date = body["date"].toString();
    const auto& driverName = body["driver_name"].toString();

    qDebug() << QString("The driver %1 ( %2 ) was blocked. %3").arg(driverName).arg(driverId).arg(date);

    _mainWindow->checkHistory();
}

void Controller::onSelectHistoryPhotos(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto& commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "select_history_photos") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"];
    const auto& recordsList = body.toList();

    _mainWindow->setCountReccordsOperator(recordsList.count());

    _groupsQueueHistory.clear();
    for (const auto& record : recordsList)
    {
        const auto& newGroup = createGroup(record.toMap());
        if (!newGroup.isNull())
        {
            _groupsQueueHistory.enqueue(newGroup);
        }
    }
    startDownloadForHistory();
}

void Controller::onSelectStatistic(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto& commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "select_statistic") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();

    const auto& dateStart = body["date_start"].toDateTime().toString("dd MMMM yyyy hh:mm");
    const auto& dateEnd = body["date_end"].toDateTime().toString("dd MMMM yyyy hh:mm");
    const auto& date = QString("%1 - %2").arg(dateStart).arg(dateEnd);

    const auto list = body["statistic"].toList();
    _mainWindow->setStatistic(date, list);
}

void Controller::onSelectTemplate(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();

    const auto& commandType = responseMap["head"].toMap()["type"].toString();
    if (QString::compare(commandType, "select_template") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto& templateList = body["template"].toList();

    QList<TemplateObjShp> photosList;
    for (const auto& templatePhoto : templateList)
    {
        const auto& templateMap = templatePhoto.toMap();
        const auto& photoUrl = templateMap["url"].toString();
        const auto& comment = templateMap["comment"].toString();

        photosList << TemplateObjShp::create(photoUrl, comment);
    }

    emit signalTemplateDownloadManager(photosList);
    emit signalTest(photosList);

//    _mainWindow->onLoadTemplate(photosList);
}

void Controller::onSelectTemplateForMain()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "select_template";

    network::RequestShp request(new network::DefaultRequest("select_template", requestMap));
    _requestsManager->execute(request, this, &Controller::onSelectTemplate);
}

void Controller::onSearchData(const network::ResponseShp response)
{
    const auto& responseMap = response->toVariant().toMap();
    const auto& listMap = responseMap["body"].toList();
    QStringList list;

    _mapUsers.clear();
    for(auto elementList : listMap)
    {
        auto map = elementList.toMap();
        QString name;
        if (map.keys().count() > 1)
        {
            name = map["name"].toString();
            _mapUsers[name] = map["id"].toLongLong();
        }
        else
            name = map[map.firstKey()].toString();
        list << name;
    }

    _mainWindow->searchList(list);
}

void Controller::runFetchTimer()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto fetchInterval = settings["FetchDataInterval"].toUInt();

    startTimer(fetchInterval, Qt::VeryCoarseTimer);
}

void Controller::timerEvent(QTimerEvent* event)
{
    killTimer(event->timerId());

    fetchNewPhotos();
}

const PhotoGroupShp Controller::createGroup(const QVariantMap& data)
{
    PhotoGroupShp group = PhotoGroupShp::create();
    group->_groupId = data["id"].toULongLong();
    group->_driverId = data["id_driver"].toULongLong();
    group->_driverName = data["driver_name"].toString();
    group->_driverNumber = data["driver_number"].toString();
    group->_autoId = data["auto_id"].toULongLong();
    group->_autoModel = data["auto_model"].toString();
    group->_autoMarka = data["auto_marka"].toString();
    group->_autoNumber = data["auto_number"].toString();
    group->_autoColor = data["auto_color"].toString();
    group->_urlPhotoDriver = QUrl(data["url_photo_driver"].toString());

    group->_autoClass = data["auto_type"].toInt();
    group->_branding = data["option_branding"].toInt();
    group->_terminal = data["option_terminal"].toInt();
    group->_chair = data["option_chair"].toInt();
    group->_chairCradle = data["option_chair_cradle"].toInt();
    group->_booster = data["option_booster"].toInt();
    group->_airport = data["option_airport"].toInt();
    group->_checker = data["option_checker"].toInt();

    auto images = data["images"].toList();
    if (images.count() == 0)
        return PhotoGroupShp(nullptr);
    group->_photoUrls.resize(images.count());
    for (const auto& photo : images)
    {
        const auto& photoMap = photo.toMap();
        const auto pos = photoMap["pos"].toULongLong();
        const auto& url = QUrl(photoMap["url"].toString());
        group->_photoUrls[pos] = url;
    }

    // NOTE: temp validation
    for (const auto& photoUrl : group->_photoUrls)
    {
        if (photoUrl.toString().contains("127.0.0.1"))
            return PhotoGroupShp(nullptr);
    }

    if (data.contains("failed_photos"))
    {
        const auto& failedPhotos = data["failed_photos"].toString();
        const auto& fPhotosList = failedPhotos.split(",", QString::SkipEmptyParts);
        for (const auto& photo : fPhotosList)
            group->_failedPhotos << photo.toUInt();
    }

    if (data.contains("status"))
        group->_status = data["status"].toUInt();

    if (data.contains("op_comment"))
        group->_comment = data["op_comment"].toString();

    if (data.contains("user_name"))
        group->_userName = data["user_name"].toString();

    group->_dateCreate = data["date_create"].toDateTime();
    if (data.contains("date_close"))
        group->_dateClose = data["date_close"].toDateTime();

    if (data.contains("verified"))
        group->_verified = data["verified"].toUInt();

    return group;
}

void Controller::startDownload()
{
    if (_groupsQueue.isEmpty())
        return;

    const auto& group = _groupsQueue.dequeue();
    if (group.isNull())
        return;

    emit signalLiveFeedDownloadManager(group);
}

void Controller::startDownloadForHistory()
{
    if (_groupsQueueHistory.isEmpty())
        return;

    const auto& group = _groupsQueueHistory.dequeue();
    emit signalHistoryDownloadManager(group);
}

void Controller::createDownloadManagers()
{
    PhotoDownloadManager *liveFeedDownloadManager = new PhotoDownloadManager();
    liveFeedDownloadManager->moveToThread(&_threadLiveFeed);
    connect(&_threadLiveFeed, &QThread::finished,
            liveFeedDownloadManager, &PhotoDownloadManager::deleteLater);
    connect(this, &Controller::signalLiveFeedDownloadManager,
            liveFeedDownloadManager, &PhotoDownloadManager::start, Qt::QueuedConnection);
    connect(liveFeedDownloadManager, &PhotoDownloadManager::end,
            this, &Controller::onEndDownload, Qt::QueuedConnection);
    connect(liveFeedDownloadManager, &PhotoDownloadManager::failed,
            this, &Controller::onFailedDownload, Qt::QueuedConnection);
    connect(liveFeedDownloadManager, &PhotoDownloadManager::signalIsBusy,
            this, &Controller::isBusy, Qt::QueuedConnection);
    _threadLiveFeed.start();

    PhotoDownloadManager *historyDownloadManager = new PhotoDownloadManager();
    historyDownloadManager->moveToThread(&_threadHistory);
    connect(&_threadHistory, &QThread::finished,
            historyDownloadManager, &PhotoDownloadManager::deleteLater);
    connect(this, &Controller::signalHistoryDownloadManager,
            historyDownloadManager, &PhotoDownloadManager::start, Qt::QueuedConnection);
    connect(historyDownloadManager, &PhotoDownloadManager::end,
            this, &Controller::onEndDownloadForHistory, Qt::QueuedConnection);
    connect(historyDownloadManager, &PhotoDownloadManager::failed,
            this, &Controller::onFailedDownloadForHistory, Qt::QueuedConnection);
    _threadHistory.start();

    PhotoDownloadManager *templateDownloadManager = new PhotoDownloadManager();
    templateDownloadManager->moveToThread(&_threadTemlate);
    connect(&_threadTemlate, &QThread::finished,
            templateDownloadManager, &PhotoDownloadManager::deleteLater);
    connect(this, &Controller::signalTemplateDownloadManager,
            templateDownloadManager, &PhotoDownloadManager::startDownloadTemplate, Qt::QueuedConnection);
    connect(this, &Controller::signalTest,
            templateDownloadManager, &PhotoDownloadManager::test, Qt::QueuedConnection);
    connect(templateDownloadManager, &PhotoDownloadManager::endTemplate,
            this, &Controller::onEndDownloadForTemplate, Qt::QueuedConnection);
    _threadTemlate.start();
}

void Controller::createMediaPlayer()
{
    QDir soundFolder = QDir::current();
    if (!soundFolder.cd("sounds"))
    {
        QMessageBox::warning(nullptr, "", soundFolder.absoluteFilePath("new_photos.wav"));
        return;
    }

    const auto soundPath = QString(soundFolder.absoluteFilePath("new_photos.wav"));
    if (!QFile::exists(soundPath))
        return;

    _newPhotosSound = new QSound(soundPath, this);
}

void Controller::playNewPhotosSound()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto soundTimeout = settings["SoundTimeout"].toLongLong();

    if (_soundTimer.elapsed() < soundTimeout)
        return;
    else
        _soundTimer.restart();

    if (_newPhotosSound)
        _newPhotosSound->play();
}

void Controller::networkAccesManager(QHttpMultiPart *multiPart)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Connection");
    const auto& host = settings["Host"].toString();
    const auto& scheme = settings["Scheme"].toString();

    QUrl url(scheme + "://" + host +"/photoValidator.php");
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkReply* reply = networkManager->post(request, multiPart);
    multiPart->setParent(reply);
    connect(networkManager, &QNetworkAccessManager::finished, this, &Controller::onGetReply);
}
