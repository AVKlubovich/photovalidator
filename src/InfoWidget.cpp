#include "Common.h"
#include "InfoWidget.h"
#include "ui_InfoWidget.h"

#include "PhotoGroup.h"
#include "ImageFullScreen.h"
#include "Models/PhotosItem.h"


using namespace photo_validator;

InfoWidget::InfoWidget(const PhotoGroupShp group, QWidget* parent)
    : QWidget(parent)
    , _ui(new Ui::InfoWidget)
    , _group(group)
{
    _ui->setupUi(this);

    fillInfo();
    createSceneImage(_group->_downloadPhotoDriver, _group->_downloadPhotos);

    connect(_ui->editComment, &QLineEdit::textChanged,
            this, &InfoWidget::checkVisibleForWidgets);

    checkVisibleForWidgets();

    _ui->btnPhotoDriver->setHidden(true);
}

InfoWidget::~InfoWidget()
{
    delete _ui;
}

void InfoWidget::onCheckValidity(const quint64 groupId)
{
    if (_group->_groupId == groupId)
        this->deleteLater();
}

void InfoWidget::checkVisibleForWidgets()
{
    bool checked = false;
    for (const auto& toolBtn : _vectorToolButton)
    {
        if (toolBtn->isChecked())
        {
            checked = true;
            toolBtn->setPalette(QPalette(QColor(Qt::red)));
        }
        else
        {
            toolBtn->setPalette(QPalette(QColor(Qt::green)));
        }
    }

    _ui->btnAccept->setVisible(!checked);
    _ui->btnInquiry->setVisible(checked);
    _ui->editComment->setVisible(checked);
}

void InfoWidget::on_btnAccept_clicked()
{
    emit accept(_group);
}

void InfoWidget::on_btnInquiry_clicked()
{
    for (auto i = 0; i < _vectorToolButton.count(); ++i)
    {
        if (_vectorToolButton.at(i)->isChecked())
        {
            _group->_failedPhotos << i;
        }
    }

    if (_group->_failedPhotos.isEmpty())
    {
        qDebug() << __FUNCTION__ << "Checked ids is empty";
        return;
    }

    _group->_comment = _ui->editComment->text();

    emit inquiry(_group);
}

void photo_validator::InfoWidget::on_btnBlock_clicked()
{
    bool checked = false;
    for (const auto& toolBtn : _vectorToolButton)
    {
        if (toolBtn->isChecked())
        {
            checked = true;
            break;
        }
    }

    if (!checked)
    {
        QMessageBox::warning(nullptr, tr("Warning!"), tr("First change the failed photos and write comment"));
        return;
    }

    for (auto i = 0; i < _vectorToolButton.count(); ++i)
    {
        if (_vectorToolButton.at(i)->isChecked())
        {
            _group->_failedPhotos << i;
        }
    }

    _group->_comment = _ui->editComment->text();

    emit blocked(_group);
}

void InfoWidget::clickOnImage(QGraphicsPixmapItem *pixmapItem)
{
    if(_vectorItems.contains(pixmapItem))
    {
        auto imageFullScreen = new ImageFullScreen(_group, _vectorItems.indexOf(pixmapItem));
        imageFullScreen->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint);
        imageFullScreen->setAttribute(Qt::WA_DeleteOnClose);
        imageFullScreen->show();
    }
}

void InfoWidget::createSceneImage(const QString &driverPhoto, const QStringList &listPathImage)
{
    int offsetX = 0;

    QGraphicsScene *scene = new QGraphicsScene(_ui->graphicsView);
    _ui->graphicsView->setScene(scene);

    const QStringList listPath = QStringList() << listPathImage << driverPhoto;

    quint8 counter = 0;

    for (const auto& pathImage : listPath)
    {
        QPixmap pixmap(pathImage);
        pixmap = pixmap.scaled(WIDTH_PIXMAP, HEIGHT_PIXMAP, Qt::KeepAspectRatio);
        PhotosItem* pixmapItem = new PhotosItem(pixmap);
        connect(pixmapItem, &PhotosItem::signalClickedByImage,
                this, &InfoWidget::clickOnImage);

        scene->addItem(pixmapItem);
        const auto offsetY = (HEIGHT_PIXMAP - pixmapItem->pixmap().size().height()) / 2;

        QToolButton *toolBtn = new QToolButton();
        toolBtn->setCheckable(true);
        toolBtn->setPalette(QPalette(QColor(Qt::green)));
        toolBtn->resize(pixmapItem->pixmap().size().width(), HEIGHT_TOOLBUTTON);

        connect(toolBtn, &QToolButton::clicked, this, &InfoWidget::checkVisibleForWidgets);

        auto btnScene = scene->addWidget(toolBtn);
        btnScene->setPos(offsetX, HEIGHT_PIXMAP + OFSET_ITEMS);
        _vectorToolButton.append(toolBtn);

        pixmapItem->setPos(offsetX, offsetY);

        if (counter == 5)
        {
            QPixmap pixmap;
            if (_group->isVerified())
                pixmap = QPixmap(":other_images/ok.png");
            else
                pixmap = QPixmap(":other_images/not_ok.png");;

            auto validationItem = scene->addPixmap(pixmap);
            validationItem->setParentItem(pixmapItem);
            validationItem->setZValue(validationItem->zValue() + 1);
        }

        _vectorItems.append(pixmapItem);

        offsetX += pixmapItem->pixmap().size().width() + OFSET_ITEMS;

        ++counter;
    }

    _ui->graphicsView->horizontalScrollBar()->setValue(1);
}

void InfoWidget::fillInfo()
{
    _ui->editDriverId->setText(QString::number(_group->_driverId));
    _ui->editDriverName->setText(_group->_driverName);
    _ui->editDriverNumber->setText(_group->_driverNumber);
    _ui->editAutoMarkaModel->setText(QString("%1 %2").arg(_group->_autoMarka).arg(_group->_autoModel));
    _ui->editAutoNumber->setText(_group->_autoNumber);
    _ui->editAutoColor->setText(_group->_autoColor);
    _ui->editAutoClass->setText(_group->getAutoClass());
    _ui->editDateCreate->setDateTime(_group->_dateCreate);

    enum AutoOptionsToolTipsEnum
    {
        Branding,
        Terminal,
        Chair,
        ChairCradle,
        Booster,
        Airport,
        Checker
    };

    QStringList autoOptionsToolTips
    {
        QObject::tr("Branding"),
        QObject::tr("Terminal"),
        QObject::tr("Chair"),
        QObject::tr("Chair cradle"),
        QObject::tr("Booster"),
        QObject::tr("Airport"),
        QObject::tr("Checker")
    };

    auto addOptionImage = [&, this](const QString& imagePath, const AutoOptionsToolTipsEnum toolTip)
    {
        QPixmap pixmap(imagePath);
        pixmap = pixmap.scaled(25, 25, Qt::IgnoreAspectRatio);
        auto pixLabel = new QLabel(this);
        pixLabel->setPixmap(pixmap);
        pixLabel->setMask(pixmap.mask());
        pixLabel->setToolTip(autoOptionsToolTips.at(toolTip));
        _ui->autoOptionsLayout->addWidget(pixLabel);
    };

    if (_group->_branding)
        addOptionImage(":auto_options/option_branding.png", Branding);

    if (_group->_terminal)
        addOptionImage(":auto_options/option_terminal.png", Terminal);

    if (_group->_chair)
        addOptionImage(":auto_options/option_chair.png", Chair);

    if (_group->_chairCradle)
        addOptionImage(":auto_options/option_chair_cradle.png", ChairCradle);

    if (_group->_booster)
        addOptionImage(":auto_options/option_booster.png", Booster);

    if (_group->_airport)
        addOptionImage(":auto_options/option_airport.png", Airport);

    if (_group->_checker)
        addOptionImage(":auto_options/option_checker.png", Checker);

    _ui->autoOptionsLayout->addSpacing(50);

    if (_group->_downloadPhotoDriver.isEmpty())
        _ui->btnPhotoDriver->setEnabled(false);

    if (_group->isVerified())
    {
        _ui->labelValidation->setPixmap(QPixmap(":other_images/ok.png"));
        _ui->labelValidation->setToolTip("Фото водителя прошло автоматическую проверку");
    }
    else
    {
        _ui->labelValidation->setPixmap(QPixmap(":other_images/not_ok.png"));
        _ui->labelValidation->setToolTip("Фото водителя не прошло автоматическую проверку");
    }
    _ui->labelValidation->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
}

void photo_validator::InfoWidget::on_btnPhotoDriver_clicked()
{
    auto imageFullScreen = new ImageFullScreen(_group->_downloadPhotoDriver);
    imageFullScreen->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint);
    imageFullScreen->setAttribute(Qt::WA_DeleteOnClose);
    imageFullScreen->show();
}
