
# PhotoValidator

##Описание проекта
Система для проверки автомобилей частных водителей

##Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)
[Qt5](https://ru.wikipedia.org/wiki/Qt)

##Системные требования
Windows; Linux

##Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

##Список контрибьюторов
[@github/avklubovich](../../../../avklubovich)
[@github/AlexandrDedckov](../../../../AlexandrDedckov)